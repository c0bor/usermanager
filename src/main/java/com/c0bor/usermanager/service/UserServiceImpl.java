package com.c0bor.usermanager.service;

import com.c0bor.usermanager.dao.UserDao;
import com.c0bor.usermanager.model.UserEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Oleksiy on 08.11.2016.
 */
@Service
public class UserServiceImpl implements UserService{

    private UserDao userDao;

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    @Transactional
    public void addUser(UserEntity user) {
        userDao.addUser(user);
    }

    @Override
    @Transactional
    public void updateUser(UserEntity user) {
        userDao.updateUser(user);
    }

    @Override
    @Transactional
    public void deleteUser(int id) {
        userDao.deleteUser(id);
    }

    @Override
    @Transactional
    public UserEntity getUserById(int id) {
        return this.userDao.getUserById(id);
    }

    @Override
    @Transactional
    public List<UserEntity> listUsers() {
        return userDao.listUsers();
    }

    @Override
    @Transactional
    public void indexUsers() throws Exception {
        userDao.indexUsers();
    }

    @Override
    @Transactional
    public List<UserEntity> searchForUser(String searchText) throws Exception {
        return userDao.searchForUser(searchText);
    }
}
