package com.c0bor.usermanager.controller;

import com.c0bor.usermanager.model.UserEntity;
import com.c0bor.usermanager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Oleksiy on 08.11.2016.
 */
@Controller
public class MainController {
    private UserService userService;

    @Autowired
    @Qualifier(value = "userService")
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "users", method = RequestMethod.GET)
    public String listUsers(@RequestParam(required = false) Integer page, Model model) throws Exception {
        userService.indexUsers(); //This action will force re-index all the full text indexes. In reality this should never be exposed to end user, but in this example, you can use this action to see the re-index in action.
        List<UserEntity> users = userService.listUsers();
        PagedListHolder<UserEntity> pagedListHolder = new PagedListHolder<UserEntity>(users);
        model.addAttribute("user", new UserEntity());

        pagedListHolder.setPageSize(5);
        model.addAttribute("maxPages", pagedListHolder.getPageCount());

        if (page == null || page < 1 || page > pagedListHolder.getPageCount())
        {
            page = 1;
            pagedListHolder.setPage(0);
        }
        else
        {
            pagedListHolder.setPage(page - 1);
        }

        model.addAttribute("page", page);
        model.addAttribute("listUsers", pagedListHolder.getPageList());

        return "users";
    }

    @RequestMapping(value = "/users/add", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("user") UserEntity user) {
        if (user.getId() == 0) {
            this.userService.addUser(user);
        } else {
            this.userService.updateUser(user);
        }
        return "redirect:/users";
    }

    @RequestMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") int id) {
        this.userService.deleteUser(id);

        return "redirect:/users";
    }

    @RequestMapping(value = "edit/{id}")
    public String editUser(@PathVariable("id") int id, @RequestParam(required = false) Integer page, Model model) {
        model.addAttribute("user", this.userService.getUserById(id));

        List<UserEntity> users = userService.listUsers();
        PagedListHolder<UserEntity> pagedListHolder = new PagedListHolder<UserEntity>(users);

        pagedListHolder.setPageSize(5);
        model.addAttribute("maxPages", pagedListHolder.getPageCount());

        if (page == null || page < 1 || page > pagedListHolder.getPageCount())
        {
            page = 1;
            pagedListHolder.setPage(0);
        }
        else
        {
            pagedListHolder.setPage(page - 1);
        }

        model.addAttribute("page", page);
        model.addAttribute("listUsers", pagedListHolder.getPageList());

        return "users";
    }

    @RequestMapping("/userDetail/{id}")
    public String userDetail(@PathVariable("id") int id, Model model) {
        model.addAttribute("user", this.userService.getUserById(id));

        return "userDetail";
    }

    @RequestMapping(value = "/doSearch", method = RequestMethod.POST)
    public String search(@RequestParam("searchText") String searchText, Model model) throws Exception
    {
        List<UserEntity> allFound = userService.searchForUser(searchText);
        model.addAttribute("listUsers", allFound);

        return "foundUsers";
    }

}
