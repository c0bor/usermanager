package com.c0bor.usermanager.dao;

import com.c0bor.usermanager.model.UserEntity;

import java.util.List;

/**
 * Created by Oleksiy on 08.11.2016.
 */
public interface UserDao {
    public void addUser(UserEntity user);

    public void updateUser(UserEntity user);

    public void deleteUser(int id);

    public UserEntity getUserById(int id);

    public List<UserEntity> listUsers();

    public void indexUsers() throws Exception;

    public List<UserEntity> searchForUser(String searchText) throws Exception;
}
