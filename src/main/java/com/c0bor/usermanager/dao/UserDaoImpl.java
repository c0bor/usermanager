package com.c0bor.usermanager.dao;

import com.c0bor.usermanager.model.UserEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Oleksiy on 08.11.2016.
 */
@Repository
@SuppressWarnings("unchecked")
public class UserDaoImpl implements UserDao {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addUser(UserEntity user) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(user);
    }

    @Override
    public void updateUser(UserEntity user) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(user);
    }

    @Override
    public void deleteUser(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        UserEntity user = getUserById(id);
        if(user!=null)
            session.delete(getUserById(id));
    }

    @Override
    public UserEntity getUserById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        return (UserEntity) session.get(UserEntity.class, new Integer(id));
    }

    @Override
    public List<UserEntity> listUsers() {
        Session session = this.sessionFactory.getCurrentSession();

        return session.createQuery("from UserEntity").list();
    }

    @Override
    public void indexUsers() throws Exception {
        try
        {
            Session session = sessionFactory.getCurrentSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            fullTextSession.createIndexer().startAndWait();
        }
        catch(Exception e)
        {
            throw e;
        }
    }

    @Override
    public List<UserEntity> searchForUser(String searchText) throws Exception {
        try
        {
            Session session = sessionFactory.getCurrentSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);

            QueryBuilder qb = fullTextSession.getSearchFactory()
                    .buildQueryBuilder().forEntity(UserEntity.class).get();
            org.apache.lucene.search.Query query = qb
                    .keyword().onFields("name", "age")
                    .matching(searchText)
                    .createQuery();

            org.hibernate.Query hibQuery =
                    fullTextSession.createFullTextQuery(query, UserEntity.class);

            return hibQuery.list();
        }
        catch(Exception e)
        {
            throw e;
        }
    }
}
