package com.c0bor.usermanager.model;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.search.annotations.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by Oleksiy on 09.11.2016.
 */
@Entity
@Indexed
@Table(name = "user")
public class UserEntity {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


    @Column(name = "name", nullable = false, length = 25)
    @Field(index = Index.YES, analyze= Analyze.YES, store= Store.NO)
    private String name;

    @Column(name = "age", nullable = false)
    @Field(index = Index.YES, analyze= Analyze.YES, store= Store.NO)
    private int age;

    @Column(name = "isAdmin", nullable = false)
    private boolean isAdmin;

    @Column(name = "createdDate", nullable = true)
    @Generated(GenerationTime.ALWAYS)
    private Timestamp createdDate = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }


    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }


    @Override
    public String toString() {
        return "UserEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", isAdmin=" + isAdmin +
                ", createdDate=" + createdDate +
                '}';
    }
}
